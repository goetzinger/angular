import { Training } from "./training.model";

export let trainingsMockList = [new Training("Angular Grundkurs", undefined, "assets/images/trainings/angularjs-shield.svg",true)
,
new Training("Angular Fortgeschrittene", "Mehr als im Grundkurs", "assets/images/trainings/angular2-shield.svg",false),
new Training("Typescript", "Javascript nur Besser", "assets/images/trainings/typescript-logo.svg",false)];
